package me.KG20.supertools.Init;

import me.KG20.supertools.Main.Constants;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

import java.util.ArrayList;
import java.util.List;

public class CreativeTabsRegistry {

    public static final DeferredRegister<CreativeModeTab> TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, Constants.MODID);

    public static final List<Item> ARMOR_ITEMS = new ArrayList<>();
    public static final List<Item> TOOLS_ITEMS = new ArrayList<>();
    public static final List<Item> SUPER_TOOLS_ITEMS = new ArrayList<>();

    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> ARMOR = TABS.register("armor",
            ()-> CreativeModeTab.builder()
                    .title(Component.translatable("itemGroup.armor"))
                    .icon(() -> new ItemStack(RegisterItems.emeraldChestplate.get().asItem()))
                    .displayItems((displayParams, output) ->
                        ARMOR_ITEMS.forEach(itemLike -> output.accept(new ItemStack(itemLike))))
                    .build()
    );

    public static DeferredHolder<CreativeModeTab, CreativeModeTab> SUPERTOOLS;

    public static void registerSuperToolsTab(){
        SUPERTOOLS = TABS.register("supertools",
                ()-> CreativeModeTab.builder()
                        .title(Component.translatable("itemGroup.supertools"))
                        .icon(() -> new ItemStack(RegisterItems.specialCup.get().asItem()))
                        .displayItems((displayParams, output) ->
                                SUPER_TOOLS_ITEMS.forEach(itemLike -> output.accept(new ItemStack(itemLike))))
                        .build()
        );
    }
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> TOOLS = TABS.register("tools",
            ()-> CreativeModeTab.builder()
                    .title(Component.translatable("itemGroup.tools"))
                    .icon(() -> new ItemStack(RegisterItems.quartzAxe.get().asItem()))
                    .displayItems((displayParams, output) ->
                            TOOLS_ITEMS.forEach(itemLike -> output.accept(new ItemStack(itemLike))))
                    .build()
    );

}
