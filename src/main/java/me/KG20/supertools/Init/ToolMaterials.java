package me.KG20.supertools.Init;

import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Blocks;
import me.KG20.supertools.Config.Config;
import net.neoforged.neoforge.common.SimpleTier;

import java.util.function.Supplier;

public class ToolMaterials {


    //TagKey<Block> incorrectBlocksForDrops, int uses, float speed, float attackDamageBonus,int enchantmentValue, Supplier<Ingredient> repairIngredient
    public static final Tier QUARTZ = new SimpleTier(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, Config.durability_Quartz.get(),6.0F, Config.attackdamage_Quartz.get().floatValue(), 14, () -> Ingredient.of(Items.QUARTZ));
    public static final Tier OBSIDIAN = new SimpleTier(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, Config.durability_Obsidian.get(), 7F, Config.attackdamage_Obsidian.get().floatValue(), 10, () -> Ingredient.of(Blocks.OBSIDIAN));
    public static final Tier EMERALD = new SimpleTier(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, Config.durability_Emerald.get(), 9.5F, Config.attackdamage_Emerald.get().floatValue(), 10, () -> Ingredient.of(Items.EMERALD));
    public static final Tier LAPIS = new SimpleTier(BlockTags.INCORRECT_FOR_STONE_TOOL, Config.durability_Lapis.get(), 6.0F, Config.attackdamage_Lapis.get().floatValue(), 20, () -> Ingredient.of(Items.LAPIS_LAZULI));
    public static final Tier COPPER = new SimpleTier(BlockTags.INCORRECT_FOR_IRON_TOOL, Config.durability_Copper.get(), 6.0F, Config.attackdamage_Copper.get().floatValue(), 10, () -> Ingredient.of(Items.COPPER_INGOT));
    public static final Tier REDSTONE = new SimpleTier(BlockTags.INCORRECT_FOR_IRON_TOOL, Config.durability_Redstone.get(), 15.0F, Config.attackdamage_Redstone.get().floatValue(), 20, () -> Ingredient.of(Items.REDSTONE));
    public static final Tier SUPERTOOLS = new SimpleTier(BlockTags.INCORRECT_FOR_NETHERITE_TOOL, Config.durability_SuperTools.get(), 10.6F, Config.attackdamage_SuperTools.get().floatValue(), 16, () -> Ingredient.of(RegisterItems.adarum.get().asItem()));
    public static final Tier CUP = new SimpleTier(BlockTags.INCORRECT_FOR_NETHERITE_TOOL, Config.durability_Cup.get(), 10.6F, Config.attackdamage_Cup.get().floatValue(), 16, () -> Ingredient.of(Items.DIAMOND));
    public static final Tier SPECIALCUP = new SimpleTier(BlockTags.INCORRECT_FOR_NETHERITE_TOOL, Config.durability_SpecialCup.get(), 12F, Config.attackdamage_SpecialCup.get().floatValue(), 16, () ->Ingredient.of(Items.DIAMOND));

}
