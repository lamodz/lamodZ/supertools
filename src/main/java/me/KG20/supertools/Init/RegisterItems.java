package me.KG20.supertools.Init;


import me.KG20.supertools.Armor.ItemArmor;
import me.KG20.supertools.Config.Config;
import me.KG20.supertools.Main.Constants;
import me.KG20.supertools.Tools.*;
import net.minecraft.world.item.*;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;


public class RegisterItems {


    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(Constants.MODID);



    public static final DeferredItem<Sickle> woodenSickle = ITEMS.register("wooden_sickle", () -> new Sickle(new Item.Properties().durability(59)));
    public static final DeferredItem<Sickle> stoneSickle = ITEMS.register("stone_sickle", () -> new Sickle(new Item.Properties().durability(131)));
    public static final DeferredItem<Sickle> ironSickle = ITEMS.register("iron_sickle", () -> new Sickle(new Item.Properties().durability(250)));
    public static final DeferredItem<Sickle> goldenSickle = ITEMS.register("golden_sickle", () -> new Sickle(new Item.Properties().durability(32)));
    public static final DeferredItem<Sickle> diamondSickle = ITEMS.register("diamond_sickle", () -> new Sickle(new Item.Properties().durability(1561)));
    public static final DeferredItem<Sickle> netheriteSickle = ITEMS.register("netherite_sickle", () -> new Sickle(new Item.Properties().durability(2031).fireResistant()));
    public static final DeferredItem<Sickle> quartzSickle = ITEMS.register("quartz_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Quartz.get())));
    public static final DeferredItem<Sickle> copperSickle = ITEMS.register("copper_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Copper.get())));
    public static final DeferredItem<Sickle> emeraldSickle = ITEMS.register("emerald_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Emerald.get())));
    public static final DeferredItem<Sickle> obsidianSickle = ITEMS.register("obsidian_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Obsidian.get())));
    public static final DeferredItem<Sickle> lapisSickle = ITEMS.register("lapis_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Lapis.get())));
    public static final DeferredItem<Sickle> redstoneSickle = ITEMS.register("redstone_sickle", () -> new Sickle(new Item.Properties().durability(Config.durability_Redstone.get())));



    public static final DeferredItem<HoeItem> quartzHoe = ITEMS.register("quartz_hoe",() -> new Hoe(ToolMaterials.QUARTZ));
    public static final DeferredItem<HoeItem> copperHoe = ITEMS.register("copper_hoe", () -> new Hoe(ToolMaterials.COPPER));
    public static final DeferredItem<HoeItem> emeraldHoe = ITEMS.register("emerald_hoe", () -> new Hoe(ToolMaterials.EMERALD));
    public static final DeferredItem<HoeItem> obsidianHoe = ITEMS.register("obsidian_hoe", () -> new Hoe(ToolMaterials.OBSIDIAN));
    public static final DeferredItem<HoeItem> lapisHoe = ITEMS.register("lapis_hoe", () -> new Hoe(ToolMaterials.LAPIS));
    public static final DeferredItem<HoeItem> redstoneHoe = ITEMS.register("redstone_hoe", () -> new Hoe(ToolMaterials.REDSTONE));

    public static final DeferredItem<AxeItem> quartzAxe = ITEMS.register("quartz_axe",() -> new Axe(ToolMaterials.QUARTZ));
    public static final DeferredItem<AxeItem> copperAxe = ITEMS.register("copper_axe",() -> new Axe(ToolMaterials.COPPER));
    public static final DeferredItem<AxeItem> emeraldAxe = ITEMS.register("emerald_axe",() -> new Axe(ToolMaterials.EMERALD));
    public static final DeferredItem<AxeItem> obsidianAxe = ITEMS.register("obsidian_axe",() -> new Axe(ToolMaterials.OBSIDIAN));
    public static final DeferredItem<AxeItem> lapisAxe = ITEMS.register("lapis_axe",() -> new Axe(ToolMaterials.LAPIS));
    public static final DeferredItem<AxeItem> redstoneAxe = ITEMS.register("redstone_axe",() -> new Axe(ToolMaterials.REDSTONE));

    public static final DeferredItem<PickaxeItem> quartzPickaxe = ITEMS.register("quartz_pickaxe",() -> new Pickaxe(ToolMaterials.QUARTZ));
    public static final DeferredItem<PickaxeItem> copperPickaxe = ITEMS.register("copper_pickaxe",() -> new Pickaxe(ToolMaterials.COPPER));
    public static final DeferredItem<PickaxeItem> emeraldPickaxe = ITEMS.register("emerald_pickaxe",() -> new Pickaxe(ToolMaterials.EMERALD));
    public static final DeferredItem<PickaxeItem> obsidianPickaxe = ITEMS.register("obsidian_pickaxe",() -> new Pickaxe(ToolMaterials.OBSIDIAN));
    public static final DeferredItem<PickaxeItem> lapisPickaxe = ITEMS.register("lapis_pickaxe",() -> new Pickaxe(ToolMaterials.LAPIS));
    public static final DeferredItem<PickaxeItem> redstonePickaxe = ITEMS.register("redstone_pickaxe",() -> new Pickaxe(ToolMaterials.REDSTONE));

    public static final DeferredItem<ShovelItem> quartzShovel = ITEMS.register("quartz_shovel",() -> new Shovel(ToolMaterials.QUARTZ));
    public static final DeferredItem<ShovelItem> copperShovel = ITEMS.register("copper_shovel",() -> new Shovel(ToolMaterials.COPPER));
    public static final DeferredItem<ShovelItem> emeraldShovel = ITEMS.register("emerald_shovel",() -> new Shovel(ToolMaterials.EMERALD));
    public static final DeferredItem<ShovelItem> obsidianShovel = ITEMS.register("obsidian_shovel",() -> new Shovel(ToolMaterials.OBSIDIAN));
    public static final DeferredItem<ShovelItem> lapisShovel = ITEMS.register("lapis_shovel",() -> new Shovel(ToolMaterials.LAPIS));
    public static final DeferredItem<ShovelItem> redstoneShovel = ITEMS.register("redstone_shovel",() -> new Shovel(ToolMaterials.REDSTONE));

    public static final DeferredItem<SwordItem> quartzSword = ITEMS.register("quartz_sword",() -> new Sword(ToolMaterials.QUARTZ));
    public static final DeferredItem<SwordItem> copperSword = ITEMS.register("copper_sword",() -> new Sword(ToolMaterials.COPPER));
    public static final DeferredItem<SwordItem> emeraldSword = ITEMS.register("emerald_sword",() -> new Sword(ToolMaterials.EMERALD));
    public static final DeferredItem<SwordItem> obsidianSword = ITEMS.register("obsidian_sword",() -> new Sword(ToolMaterials.OBSIDIAN));
    public static final DeferredItem<SwordItem> lapisSword = ITEMS.register("lapis_sword",() -> new Sword(ToolMaterials.LAPIS));
    public static final DeferredItem<SwordItem> redstoneSword = ITEMS.register("redstone_sword",() -> new Sword(ToolMaterials.REDSTONE));



    public static final DeferredItem<ArmorItem> emeraldHelmet = ITEMS.register("emerald_helmet", () -> new ItemArmor(ArmorMaterials.emeraldArmor, ArmorItem.Type.HELMET, new Item.Properties().durability(ArmorItem.Type.HELMET.getDurability(33))));
    public static final DeferredItem<ArmorItem> obsidianHelmet = ITEMS.register("obsidian_helmet", () -> new ItemArmor(ArmorMaterials.obsidianArmor, ArmorItem.Type.HELMET, new Item.Properties().durability(ArmorItem.Type.HELMET.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> lapisHelmet = ITEMS.register("lapis_helmet", () -> new ItemArmor(ArmorMaterials.lapisArmor, ArmorItem.Type.HELMET, new Item.Properties().durability(ArmorItem.Type.HELMET.getDurability(10))));
    public static final DeferredItem<ArmorItem> quartzHelmet = ITEMS.register("quartz_helmet", () -> new ItemArmor(ArmorMaterials.quartzArmor, ArmorItem.Type.HELMET, new Item.Properties().durability(ArmorItem.Type.HELMET.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> copperHelmet = ITEMS.register("copper_helmet", () -> new ItemArmor(ArmorMaterials.copperArmor, ArmorItem.Type.HELMET, new Item.Properties().durability(ArmorItem.Type.HELMET.getDurability(10))));

    public static final DeferredItem<ArmorItem> emeraldChestplate = ITEMS.register("emerald_chestplate", () -> new ItemArmor(ArmorMaterials.emeraldArmor, ArmorItem.Type.CHESTPLATE, new Item.Properties().durability(ArmorItem.Type.CHESTPLATE.getDurability(33))));
    public static final DeferredItem<ArmorItem> obsidianChestplate = ITEMS.register("obsidian_chestplate", () -> new ItemArmor(ArmorMaterials.obsidianArmor, ArmorItem.Type.CHESTPLATE, new Item.Properties().durability(ArmorItem.Type.CHESTPLATE.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> lapisChestplate = ITEMS.register("lapis_chestplate", () -> new ItemArmor(ArmorMaterials.lapisArmor, ArmorItem.Type.CHESTPLATE, new Item.Properties().durability(ArmorItem.Type.CHESTPLATE.getDurability(10))));
    public static final DeferredItem<ArmorItem> quartzChestplate = ITEMS.register("quartz_chestplate", () -> new ItemArmor(ArmorMaterials.quartzArmor, ArmorItem.Type.CHESTPLATE, new Item.Properties().durability(ArmorItem.Type.CHESTPLATE.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> copperChestplate = ITEMS.register("copper_chestplate", () -> new ItemArmor(ArmorMaterials.copperArmor, ArmorItem.Type.CHESTPLATE, new Item.Properties().durability(ArmorItem.Type.CHESTPLATE.getDurability(10))));

    public static final DeferredItem<ArmorItem> emeraldLeggings = ITEMS.register("emerald_leggings", () -> new ItemArmor(ArmorMaterials.emeraldArmor, ArmorItem.Type.LEGGINGS, new Item.Properties().durability(ArmorItem.Type.LEGGINGS.getDurability(33))));
    public static final DeferredItem<ArmorItem> obsidianLeggings = ITEMS.register("obsidian_leggings", () -> new ItemArmor(ArmorMaterials.obsidianArmor, ArmorItem.Type.LEGGINGS, new Item.Properties().durability(ArmorItem.Type.LEGGINGS.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> lapisLeggings = ITEMS.register("lapis_leggings", () -> new ItemArmor(ArmorMaterials.lapisArmor, ArmorItem.Type.LEGGINGS, new Item.Properties().durability(ArmorItem.Type.LEGGINGS.getDurability(10))));
    public static final DeferredItem<ArmorItem> quartzLeggings = ITEMS.register("quartz_leggings", () -> new ItemArmor(ArmorMaterials.quartzArmor, ArmorItem.Type.LEGGINGS, new Item.Properties().durability(ArmorItem.Type.LEGGINGS.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> copperLeggings = ITEMS.register("copper_leggings", () -> new ItemArmor(ArmorMaterials.copperArmor, ArmorItem.Type.LEGGINGS, new Item.Properties().durability(ArmorItem.Type.LEGGINGS.getDurability(10))));

    public static final DeferredItem<ArmorItem> emeraldBoots = ITEMS.register("emerald_boots", () -> new ItemArmor(ArmorMaterials.emeraldArmor, ArmorItem.Type.BOOTS, new Item.Properties().durability(ArmorItem.Type.BOOTS.getDurability(33))));
    public static final DeferredItem<ArmorItem> obsidianBoots = ITEMS.register("obsidian_boots", () -> new ItemArmor(ArmorMaterials.obsidianArmor, ArmorItem.Type.BOOTS, new Item.Properties().durability(ArmorItem.Type.BOOTS.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> lapisBoots = ITEMS.register("lapis_boots", () -> new ItemArmor(ArmorMaterials.lapisArmor, ArmorItem.Type.BOOTS, new Item.Properties().durability(ArmorItem.Type.BOOTS.getDurability(10))));
    public static final DeferredItem<ArmorItem> quartzBoots = ITEMS.register("quartz_boots", () -> new ItemArmor(ArmorMaterials.quartzArmor, ArmorItem.Type.BOOTS, new Item.Properties().durability(ArmorItem.Type.BOOTS.getDurability(33)).fireResistant()));
    public static final DeferredItem<ArmorItem> copperBoots = ITEMS.register("copper_boots", () -> new ItemArmor(ArmorMaterials.copperArmor, ArmorItem.Type.BOOTS, new Item.Properties().durability(ArmorItem.Type.BOOTS.getDurability(10))));


    public static DeferredItem<BonemealTool> boneMealTool;
    public static DeferredItem<HoeItem> superHoe;
    public static DeferredItem<AxeItem> superAxe;
    public static DeferredItem<PickaxeItem> superPickaxe;
    public static DeferredItem<ShovelItem> superShovel;
    public static DeferredItem<SwordItem> superSword;
    public static DeferredItem<Cups> cup;
    public static DeferredItem<Cups> specialCup;
    public static DeferredItem<Sickle> superSickle;
    public static DeferredItem<Item> adarum;




    public static void initRegisterItems(){
        if(Config.enable_SuperTools.get()) {
            addSuperTools();
        }
        addArmor();
        addTools();
    }

    public static void registerSuperTools(){
        boneMealTool = ITEMS.register("bone_meal_tool", () -> new BonemealTool());
        superAxe = ITEMS.register("super_axe",() -> new Axe(ToolMaterials.SUPERTOOLS));
        superPickaxe = ITEMS.register("super_pickaxe",() -> new Pickaxe(ToolMaterials.SUPERTOOLS));
        superHoe = ITEMS.register("super_hoe",() -> new Hoe(ToolMaterials.SUPERTOOLS));
        superShovel = ITEMS.register("super_shovel",() -> new Shovel(ToolMaterials.SUPERTOOLS));
        superSword = ITEMS.register("super_sword",() -> new Sword(ToolMaterials.SUPERTOOLS));

        cup = ITEMS.register("cup",() -> new Cups(ToolMaterials.CUP));
        specialCup = ITEMS.register("special_cup",() -> new Cups(ToolMaterials.SPECIALCUP));
        superSickle = ITEMS.register("super_sickle",() -> new Sickle(new Item.Properties().durability(Config.durability_SuperTools.get())));
        adarum = ITEMS.register("adarum",() -> new Item(new Item.Properties().stacksTo(64)));
    }

    private static void addSuperTools(){
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(boneMealTool));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superHoe));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superAxe));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superPickaxe));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superShovel));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superSword));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(cup));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(specialCup));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(superSickle));
        CreativeTabsRegistry.SUPER_TOOLS_ITEMS.add(getItemFromDeferredItem(adarum));
    }

    private static void addTools(){
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(woodenSickle));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(stoneSickle));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(ironSickle));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(goldenSickle));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(diamondSickle));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(netheriteSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzPickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(quartzSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperPickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(copperSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldPickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(emeraldSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianPickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(obsidianSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisPickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(lapisSickle));
        
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstonePickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneSickle));

        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneHoe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneAxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstonePickaxe));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneShovel));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneSword));
        CreativeTabsRegistry.TOOLS_ITEMS.add(getItemFromDeferredItem(redstoneSickle));
    }

    private static void addArmor(){
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(emeraldHelmet));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(emeraldChestplate));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(emeraldLeggings));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(emeraldBoots));

        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(obsidianHelmet));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(obsidianChestplate));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(obsidianLeggings));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(obsidianBoots));

        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(lapisHelmet));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(lapisChestplate));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(lapisLeggings));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(lapisBoots));

        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(quartzHelmet));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(quartzChestplate));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(quartzLeggings));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(quartzBoots));

        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(copperHelmet));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(copperChestplate));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(copperLeggings));
        CreativeTabsRegistry.ARMOR_ITEMS.add(getItemFromDeferredItem(copperBoots));
    }

    private static Item getItemFromDeferredItem(DeferredItem<? extends Item> item){
        return item.get().asItem();
    }
}


