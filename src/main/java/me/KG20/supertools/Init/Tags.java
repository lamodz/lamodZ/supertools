package me.KG20.supertools.Init;

import me.KG20.supertools.Main.Constants;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;

public class Tags {
    public static class Blocks{
        public static final TagKey<Block> MINEABLE_WITH_CUP = createTag("mineable/cup");
        private static TagKey<Block> createTag(String name){
            return BlockTags.create(ResourceLocation.fromNamespaceAndPath(Constants.MODID, name));
        }
    }
}
