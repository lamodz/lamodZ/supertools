package me.KG20.supertools.Armor;

import me.KG20.supertools.Config.Config;
import me.KG20.supertools.Init.RegisterItems;
import net.minecraft.core.Holder;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.common.Tags;

public class ItemArmor extends ArmorItem {


    public ItemArmor(Holder<ArmorMaterial> pMaterial, Type pType, Properties pProperties) {
        super(pMaterial, pType, pProperties);
    }

    @Override
    public void inventoryTick(ItemStack pStack, Level pLevel, Entity pEntity, int pSlotId, boolean pIsSelected) {
        if(!pLevel.isClientSide && pEntity instanceof Player player){
            Item obsidianHelmet = RegisterItems.obsidianHelmet.get().asItem();
            Item obsidianChestplate = RegisterItems.obsidianChestplate.get().asItem();
            Item obsidianLeggings = RegisterItems.obsidianLeggings.get().asItem();
            Item obsidianBoots = RegisterItems.obsidianBoots.get().asItem();

            Item quartzHelmet = RegisterItems.quartzHelmet.get().asItem();
            Item quartzChestplate = RegisterItems.quartzChestplate.get().asItem();
            Item quartzLeggings = RegisterItems.quartzLeggings.get().asItem();
            Item quartzBoots = RegisterItems.quartzBoots.get().asItem();


            if(Config.enable_ArmorStatusEffect.get()){

                if(Config.enable_ObsidianStatusEffects.get()){

                    if(Config.enable_ObsidianStatusEffectsLevel1.get()){
                        if(player.getItemBySlot(EquipmentSlot.FEET).getItem().equals(obsidianBoots) || player.getItemBySlot(EquipmentSlot.LEGS).getItem().equals(obsidianLeggings) ||
                                player.getItemBySlot(EquipmentSlot.CHEST).getItem().equals(obsidianChestplate) || player.getItemBySlot(EquipmentSlot.HEAD).getItem().equals(obsidianHelmet)){
                            player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 10, 0)); //Slowness
                        }

                        if(Config.enable_ObsidianStatusEffectsLevel2.get()){
                            if(player.getItemBySlot(EquipmentSlot.FEET).getItem().equals(obsidianBoots) && player.getItemBySlot(EquipmentSlot.LEGS).getItem().equals(obsidianLeggings) &&
                                    player.getItemBySlot(EquipmentSlot.CHEST).getItem().equals(obsidianChestplate) && player.getItemBySlot(EquipmentSlot.HEAD).getItem().equals(obsidianHelmet)) {

                                player.addEffect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, 10, 0)); //Fire Resistance
                                player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 10, 1)); //Slowness II
                                player.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 10, 0)); //Resistance
                            }
                        }
                    }
                }
                if(Config.enable_QuartzStatusEffects.get()){
                    if(player.getItemBySlot(EquipmentSlot.FEET).getItem().equals(quartzBoots) && player.getItemBySlot(EquipmentSlot.LEGS).getItem().equals(quartzLeggings) &&
                            player.getItemBySlot(EquipmentSlot.CHEST).getItem().equals(quartzChestplate) && player.getItemBySlot(EquipmentSlot.HEAD).getItem().equals(quartzHelmet)) {
                        player.addEffect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, 10, 0)); //Fire Resistance
                    }
                }
            }
        }
        super.inventoryTick(pStack, pLevel, pEntity, pSlotId, pIsSelected);
    }

}
