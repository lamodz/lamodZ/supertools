package me.KG20.supertools.Main;

import com.mojang.logging.LogUtils;
import me.KG20.supertools.Config.Config;
import me.KG20.supertools.Event.EventHandler;
import me.KG20.supertools.Init.ArmorMaterials;
import me.KG20.supertools.Init.CreativeTabsRegistry;
import me.KG20.supertools.Init.RegisterItems;
import net.minecraft.world.item.Items;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.IConfigSpec;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLPaths;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.common.NeoForge;
import org.slf4j.Logger;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
@Mod(Constants.MODID)
public class SuperTools
{

    // Directly reference a slf4j logger
    private static final Logger LOGGER = LogUtils.getLogger();
    public SuperTools(IEventBus modEventBus, ModContainer container)
    {
        container.registerConfig(ModConfig.Type.STARTUP, Config.SPEC);
        modEventBus.addListener(this::commonSetup);

        CreativeTabsRegistry.TABS.register(modEventBus);
        if(Config.enable_SuperTools.get()){
            CreativeTabsRegistry.registerSuperToolsTab();
        }

        RegisterItems.ITEMS.register(modEventBus);
        if(Config.enable_SuperTools.get()){
            RegisterItems.registerSuperTools();
        }
        NeoForge.EVENT_BUS.register(EventHandler.class);
        modEventBus.addListener(this::addCreative);
    }

    private void commonSetup(final FMLCommonSetupEvent event)
    {

    }
    private void addCreative(BuildCreativeModeTabContentsEvent event)
    {
        RegisterItems.initRegisterItems();
    }
}


