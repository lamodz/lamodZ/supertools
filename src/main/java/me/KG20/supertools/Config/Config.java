package me.KG20.supertools.Config;

import net.neoforged.neoforge.common.ModConfigSpec;

public class Config {

    private static final ModConfigSpec.Builder BUILDER = new ModConfigSpec.Builder();

    public static final ModConfigSpec.ConfigValue<String> string;
    public static final ModConfigSpec.BooleanValue enable_SuperTools;
    public static final ModConfigSpec.BooleanValue enable_ArmorStatusEffect;
    public static final ModConfigSpec.BooleanValue enable_QuartzStatusEffects;
    public static final ModConfigSpec.BooleanValue enable_ObsidianStatusEffects;
    public static final ModConfigSpec.BooleanValue enable_ObsidianStatusEffectsLevel1;
    public static final ModConfigSpec.BooleanValue enable_ObsidianStatusEffectsLevel2;
    public static final ModConfigSpec.BooleanValue enable_BlockDropsInCreative;
    public static final ModConfigSpec.IntValue durability_Quartz;
    public static final ModConfigSpec.IntValue durability_Copper;
    public static final ModConfigSpec.IntValue durability_Obsidian;
    public static final ModConfigSpec.IntValue durability_Emerald;
    public static final ModConfigSpec.IntValue durability_Lapis;
    public static final ModConfigSpec.IntValue durability_Redstone;
    public static final ModConfigSpec.IntValue durability_SuperTools;
    public static final ModConfigSpec.IntValue durability_BoneMealTool;
    public static final ModConfigSpec.IntValue durability_Cup;
    public static final ModConfigSpec.IntValue durability_SpecialCup;
    public static final ModConfigSpec.IntValue max_wood_logs;
    public static final ModConfigSpec.DoubleValue attackdamage_Quartz;
    public static final ModConfigSpec.DoubleValue attackdamage_Copper;
    public static final ModConfigSpec.DoubleValue attackdamage_Obsidian;
    public static final ModConfigSpec.DoubleValue attackdamage_Emerald;
    public static final ModConfigSpec.DoubleValue attackdamage_Lapis;
    public static final ModConfigSpec.DoubleValue attackdamage_Redstone;
    public static final ModConfigSpec.DoubleValue attackdamage_SuperTools;
    public static final ModConfigSpec.DoubleValue attackdamage_Cup;
    public static final ModConfigSpec.DoubleValue attackdamage_SpecialCup;

    //

    static{

        BUILDER.comment("Config File of the Super Tools mod.");

        BUILDER.push("SuperTools");
        enable_SuperTools = BUILDER.comment("Enable Super Tools(Not the mod) (Default: true)").define("supertools", true);
        enable_BlockDropsInCreative = BUILDER.comment("Enable that the Super Tools drop Blocks in Creative Mode(Default: false)").define("creative_block_drops", false);

        BUILDER.push("Armor Effects");
        enable_ArmorStatusEffect = BUILDER.comment("Enable the Armor Potion Effects (Default: true)").define("armor_potion_effects", true);
        BUILDER.push("Quartz Effects");
        enable_QuartzStatusEffects = BUILDER.comment("Enable the Quartz Armor Potion Effects (Default: true)").define("quartz_armor_potion_effects", true);
        BUILDER.pop();
        BUILDER.push("Obsidian Effetcs");
        enable_ObsidianStatusEffects = BUILDER.comment("Enable the Obsidian Armor Potion Effects (Default: true)").define("obsidian_armor_potion_effects", true);
        BUILDER.push("Level 1");
        enable_ObsidianStatusEffectsLevel1 = BUILDER.comment("Enable the Obsidian Armor Potion Effects when you have only 1 piece or more pieces (Default: true)").define("obsidian_armor_potion_effects_level1", true);
        BUILDER.push("Level 2");
        enable_ObsidianStatusEffectsLevel2 = BUILDER.comment("Enable the Armor Status Effects when you have every piece(Default: true)").define("obsidian_armor_potion_effects_level2", true);
        BUILDER.pop();
        BUILDER.pop();
        BUILDER.pop();

        BUILDER.pop();
        BUILDER.push("Durabilities");
        durability_Quartz = BUILDER.comment("Quartz durabilty (Default: 1000)").defineInRange("quartz_tools_durabilty", 1000, 0, 999999999);
        durability_Copper = BUILDER.comment("Copper durabilty (Default: 200)").defineInRange("copper_tools_durabilty", 200, 0, 999999999);
        durability_Obsidian = BUILDER.comment("Obsidian durabilty (Default: 2000)").defineInRange("obsidian_tools_durabilty", 2000, 0, 999999999);
        durability_Emerald = BUILDER.comment("Emerald durabilty (Default: 1561)").defineInRange("emerald_tools_durabilty", 1561, 0, 999999999);
        durability_Lapis = BUILDER.comment("Lapis durabilty (Default: 150)").defineInRange("lapis_tools_durabilty", 150, 0, 999999999);
        durability_Redstone = BUILDER.comment("Redstone durabilty (Default: 100)").defineInRange("redstone_tools_durabilty", 100, 0, 999999999);
        durability_SuperTools = BUILDER.comment("Super Tools durabilty (Default: 2000)").defineInRange("super_tools_durabilty", 2000, 0, 999999999);
        durability_BoneMealTool = BUILDER.comment("A Bag of Bonemeal durabilty (Default: 100)").defineInRange("a_bag_of_bonemeal_durabilty", 100, 0, 999999999);
        durability_Cup = BUILDER.comment("Cup durabilty (Default: 1700)").defineInRange("cup_durabilty", 1700, 0, 999999999);
        durability_SpecialCup = BUILDER.comment("SpecialCup durabilty (Default: 2000)").defineInRange("special_cup_durabilty", 2000, 0, 999999999);
        BUILDER.pop();
        BUILDER.push("Tool Attack Damage");
        BUILDER.push("DONT CHANGE THE STRINGS!");
        string = BUILDER.define("dontchange", "\t\t\tBase Axe damage: 6\n" +
                "\t\t\tBase Sword damage: 3\n" +
                "\t\t\tBase Shovel damage: 1.5\n" +
                "\t\t\tBase Pickaxe damage: 1\n" +
                "\t\t\tBase Hoe damage: 0\n" +
                "\t\t\tBase Cup damage: 1\n" +
                "\t\t\tTo get the damage you want add the base damage of the Tool and the base attack damage of the material.");
        BUILDER.pop();
        BUILDER.push("Attack Damage");
        attackdamage_Quartz = BUILDER.comment("Quartz base attack damage").defineInRange("attackdamage_quartz", 2.0,-999999999, 999999999);
        attackdamage_Copper = BUILDER.comment("Copper base attack damage").defineInRange("attackdamage_copper", 1.5,-999999999, 999999999);
        attackdamage_Obsidian = BUILDER.comment("Obsidian base attack damage").defineInRange("attackdamage_obsidian", 3.5,-999999999, 999999999);
        attackdamage_Emerald = BUILDER.comment("Emerald base attack damage").defineInRange("attackdamage_emerald", 3.5,-999999999, 999999999);
        attackdamage_Lapis = BUILDER.comment("Lapis base attack damage").defineInRange("attackdamage_lapis", 2.0,-999999999, 999999999);
        attackdamage_Redstone = BUILDER.comment("Redstone base attack damage").defineInRange("attackdamage_redstone", -0.5,-999999999, 999999999);
        attackdamage_SuperTools = BUILDER.comment("Super Tools base attack damage").defineInRange("attackdamage_supertools", 3.5,-999999999, 999999999);
        attackdamage_Cup = BUILDER.comment("Cup base attack damage").defineInRange("attackdamage_cup", 3.5,-999999999, 999999999);
        attackdamage_SpecialCup = BUILDER.comment("Special Cup base attack damage").defineInRange("attackdamage_specialcup", 4.0,-999999999, 999999999);
        BUILDER.pop(2);

        BUILDER.push("Super Axe");
        max_wood_logs = BUILDER.comment("Set the Maxmium amount of Wood Logs being chopped with the Super Axe").defineInRange("max_wood_logs", 64, 0, 999999999);
    }

    public static final ModConfigSpec SPEC = BUILDER.build();


}
