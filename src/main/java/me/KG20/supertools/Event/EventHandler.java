package me.KG20.supertools.Event;

import me.KG20.supertools.Config.Config;
import me.KG20.supertools.Init.RegisterItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.ItemEnchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;
import net.neoforged.neoforge.event.level.BlockEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class EventHandler {

    @SubscribeEvent
    public static void SuperAxeDestroyedBlock(BlockEvent.BreakEvent event){
        Level world = event.getPlayer().level();
        Player player = event.getPlayer();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();
        BlockState state = world.getBlockState(pos);
        if(!RegisterItems.superAxe.get().asItem().equals(stack.getItem())){
            return;
        }
        Block startLog = state.getBlock();
        BlockPos currentPos = pos;
        ArrayList<BlockPos> brokenBlocks = new ArrayList<>();
        ArrayList<BlockPos> nextPos = new ArrayList<>();
        int blocksHarvested = 0;
        Random random = new Random();

        ItemEnchantments enchantments = stack.getTagEnchantments();

        if(RegisterItems.superAxe.get().asItem().equals(stack.getItem()) && startLog.defaultBlockState().is(BlockTags.LOGS) || RegisterItems.superAxe.get().asItem().equals(stack.getItem()) && startLog.defaultBlockState().is(BlockTags.LOGS_THAT_BURN)){
            brokenBlocks.add(pos);
            while(stack.getDamageValue() != stack.getMaxDamage() && blocksHarvested <= Config.max_wood_logs.get()){
                ArrayList<BlockPos> logNeighbours = getWoodNeighbours(world,currentPos,startLog, stack);
                logNeighbours.removeAll(brokenBlocks);
                if(!logNeighbours.isEmpty()){
                    for(BlockPos blockPos : logNeighbours){
                        brokenBlocks.add(blockPos);
                        nextPos.add(blockPos);
                        if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                            world.destroyBlock(blockPos, true);
                        }else{
                            world.destroyBlock(blockPos, false);
                        }

                        blocksHarvested++;

                        if(!player.isCreative()){
                            if(!enchantments.isEmpty()){
                                if(enchantments.toString().contains("Unbreaking}=>1}")){
                                    if(random.nextInt(100) + 1 <= 50){
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }else if(enchantments.toString().contains("Unbreaking}=>2}")){
                                    if(random.nextInt(100) + 1 <= 33){
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }else if(enchantments.toString().contains("Unbreaking}=>3}")){
                                    if(random.nextInt(100) + 1 <= 25){
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }
                            }else{
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        }
                    }
                }
                if(!nextPos.isEmpty()){
                    currentPos = nextPos.get(0);
                    nextPos.remove(currentPos);
                }else
                    break;
            }
        }
    }

    @SubscribeEvent
    public static void SuperPickaxeDestroyedBlock(BlockEvent.BreakEvent event) {
        Level world = event.getPlayer().level();
        Player player = event.getPlayer();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();
        int blocksCleared = 0;

        if (RegisterItems.superPickaxe.get().asItem().equals(stack.getItem())) {
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();
            Direction headRot = player.getMotionDirection();
            Random random = new Random();
            String unbreakingEnchantment = "";
            String silktouchEnchantment = "";
            String fortuneEnchantment = "";

            ItemEnchantments enchantments = stack.getTagEnchantments();
            if(!enchantments.isEmpty()){
                for (int i = 0; i < enchantments.size(); i++) {
                    if (enchantments.toString().contains("minecraft:unbreaking")) {
                        unbreakingEnchantment = enchantments.toString();
                    } else if (enchantments.toString().contains("minecraft:silk_touch")) {
                        silktouchEnchantment = enchantments.toString();
                    } else if (enchantments.toString().contains("minecraft:fortune")) {
                        fortuneEnchantment = enchantments.toString();
                    }
                }
            }
            if(world.getBlockState(pos).is(BlockTags.MINEABLE_WITH_PICKAXE)){
                if (player.getLookAngle().y <= -0.52f || player.getLookAngle().y >= 0.52f) {
                    for (int x = -1; x < 2; x++) {
                        for (int z = -1; z < 2; z++) {
                            if (stack.getDamageValue() >= stack.getMaxDamage() - 1) {
                                stack.shrink(1);
                                break;
                            }
                            int randomNumber = random.nextInt(100) + 1;
                            BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                            destroyPickaxeBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, fortuneEnchantment, silktouchEnchantment, newBlockPos, randomNumber, random);
                            blocksCleared++;
                        }
                    }
                }else if (headRot.equals(Direction.NORTH) || headRot.equals(Direction.SOUTH)) {
                    for (int x = -1; x < 2; x++) {
                        for (int y = -1; y < 2; y++) {
                            if (stack.getDamageValue() >= stack.getMaxDamage() - 1) {
                                stack.shrink(1);
                                break;
                            }
                            int randomNumber = random.nextInt(100) + 1;
                            BlockPos newBlockPos = new BlockPos(bx + x, by + y, bz);
                            destroyPickaxeBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, fortuneEnchantment, silktouchEnchantment, newBlockPos, randomNumber, random);
                            blocksCleared++;
                        }
                    }
                }else if (headRot.equals(Direction.WEST) || headRot.equals(Direction.EAST)) {
                    for (int z = -1; z < 2; z++) {
                        for (int y = -1; y < 2; y++) {
                            if (stack.getDamageValue() >= stack.getMaxDamage() - 1) {
                                stack.shrink(1);
                                break;
                            }
                            int randomNumber = random.nextInt(100) + 1;
                            BlockPos newBlockPos = new BlockPos(bx, by + y, bz + z);
                            destroyPickaxeBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, fortuneEnchantment, silktouchEnchantment, newBlockPos, randomNumber, random);
                            blocksCleared++;
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void SuperShovelDestroyedBlock(BlockEvent.BreakEvent event){
        Player player = event.getPlayer();
        Level world = player.level();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();
        Direction headRot = player.getMotionDirection();
        int blocksCleared = 0;

        if(RegisterItems.superShovel.get().asItem().equals(stack.getItem())) {
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();

            Random random = new Random();
            String unbreakingEnchantment = "";
            String silktouchEnchantment = "";

            ItemEnchantments enchantments = stack.getTagEnchantments();
            if(!enchantments.isEmpty()){
                for (int i = 0; i < enchantments.size(); i++) {
                    if (enchantments.toString().contains("minecraft:unbreaking")) {
                        unbreakingEnchantment = enchantments.toString();
                    } else if (enchantments.toString().contains("minecraft:silk_touch")) {
                        silktouchEnchantment = enchantments.toString();
                    }
                }
            }

            if(world.getBlockState(pos).is(BlockTags.MINEABLE_WITH_SHOVEL)){
                if (player.getLookAngle().y <= -0.50f || player.getLookAngle().y >= 0.50f) {
                    for (int x = -1; x < 2; x++) {
                        for (int z = -1; z < 2; z++) {
                            if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                                stack.shrink(1);
                                break;
                            }
                            BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                            destroyShovelBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, silktouchEnchantment, newBlockPos, random);
                            blocksCleared++;

                        }
                    }
                } else if (headRot.equals(Direction.NORTH) || headRot.equals(Direction.SOUTH)) {
                    for (int x = -1; x < 2; x++) {
                        for (int y = -1; y < 2; y++) {
                            if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                                stack.shrink(1);
                                break;
                            }
                            BlockPos newBlockPos = new BlockPos(bx + x, by + y, bz);
                            destroyShovelBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, silktouchEnchantment, newBlockPos, random);
                            blocksCleared++;
                        }
                    }
                } else if (headRot.equals(Direction.WEST) || headRot.equals(Direction.EAST)) {
                    for (int z = -1; z < 2; z++) {
                        for (int y = -1; y < 2; y++) {
                            if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                                stack.shrink(1);
                                break;
                            }
                            BlockPos newBlockPos = new BlockPos(bx, by + y, bz + z);
                            destroyShovelBlocks(world, player, stack, blocksCleared, unbreakingEnchantment, silktouchEnchantment, newBlockPos, random);
                            blocksCleared++;
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void SuperShovelRightClickBlock(PlayerInteractEvent.RightClickBlock event){
        //TODO: Pathable blocks as Tag
        Player player = event.getEntity();
        Level world = player.level();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();
        ArrayList<Block> pathBlocks = new ArrayList<>();
        pathBlocks.add(Blocks.DIRT);
        pathBlocks.add(Blocks.GRASS_BLOCK);
        pathBlocks.add(Blocks.ROOTED_DIRT);
        pathBlocks.add(Blocks.COARSE_DIRT);
        pathBlocks.add(Blocks.MYCELIUM);
        if (RegisterItems.superShovel.get().asItem().equals(stack.getItem())) {
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();

            Random random = new Random();
            String unbreakingEnchantment = "";
            ItemEnchantments enchantments = stack.getTagEnchantments();
            if(!enchantments.isEmpty()){
                for (int i = 0; i < enchantments.size(); i++) {
                    if (enchantments.toString().contains("minecraft:unbreaking")) {
                        unbreakingEnchantment = enchantments.toString();
                    }
                }
            }
            if(pathBlocks.contains(world.getBlockState(pos).getBlock())) {
                for (int x = -1; x < 2; x++) {
                    for (int z = -1; z < 2; z++) {
                        if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                            stack.shrink(1);
                            break;
                        }
                        BlockPos newBlockPos = new BlockPos(bx + x, by, bz+z);
                        if (world.getBlockState(new BlockPos(bx + x, by + 1, bz + z)).getBlock() == Blocks.AIR) {
                            if (pathBlocks.contains(world.getBlockState(newBlockPos).getBlock())) {
                                world.setBlockAndUpdate(newBlockPos, Blocks.DIRT_PATH.defaultBlockState());
                                if (!player.isCreative()) {
                                    if(!unbreakingEnchantment.isEmpty()){
                                        if(unbreakingEnchantment.contains("Unbreaking}=>1}")){
                                            if(random.nextInt(100) + 1 <= 50){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>2}")){
                                            if(random.nextInt(100) + 1 <= 33){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>3}")){
                                            if(random.nextInt(100) + 1 <= 25){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }
                                    }else{
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void SuperSickleRightClockBlock(PlayerInteractEvent.RightClickBlock event){
        Player player = event.getEntity();
        Level world = player.level();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();

        if(RegisterItems.superSickle.get().asItem().equals(stack.getItem())){
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();

            if (world.getBlockState(pos).getBlock() instanceof BushBlock){
                for (int x = -4; x < 5; x++) {
                    for (int z = -4; z < 5; z++) {
                        BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                        if (world.getBlockState(newBlockPos).getBlock() instanceof BushBlock) {
                            world.destroyBlock(newBlockPos, true);
                            stack.setDamageValue(stack.getDamageValue() + 1);
                            if(stack.getDamageValue() >= stack.getMaxDamage()){
                                stack.shrink(1);
                            }
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void SuperHoeRightClickBlock(PlayerInteractEvent.RightClickBlock event){
        Player player = event.getEntity();
        Level world = player.level();
        BlockPos pos = event.getPos();
        ItemStack stack = player.getMainHandItem();

        if (RegisterItems.superHoe.get().asItem().equals(stack.getItem())) {
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();

            Random random = new Random();
            String unbreakingEnchantment = "";

            ItemEnchantments enchantments = stack.getTagEnchantments();
            if(!enchantments.isEmpty()){
                if (enchantments.toString().contains("minecraft:unbreaking")) {
                    unbreakingEnchantment = enchantments.toString();
                }
            }

            if(world.getBlockState(pos).getBlock() == Blocks.DIRT || world.getBlockState(pos).getBlock() == Blocks.GRASS_BLOCK || world.getBlockState(pos).getBlock() == Blocks.DIRT_PATH || world.getBlockState(pos).getBlock().equals(Blocks.COARSE_DIRT)){
                for (int x = -1; x < 2; x++) {
                    for (int z = -1; z < 2; z++) {
                        BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                        if(newBlockPos.equals(pos)){
                            continue;
                        }
                        if(stack.getDamageValue() >= stack.getMaxDamage()){
                            stack.shrink(1);
                            break;
                        }
                        if(world.getBlockState(new BlockPos(bx + x,by + 1,bz + z)).getBlock() == Blocks.AIR){
                            //TODO: Farmland blocks as Tag
                            if (world.getBlockState(newBlockPos).getBlock().equals(Blocks.DIRT) || world.getBlockState(newBlockPos).getBlock().equals(Blocks.GRASS_BLOCK) || world.getBlockState(newBlockPos).getBlock() == Blocks.DIRT_PATH ) {
                                world.setBlockAndUpdate(newBlockPos, Blocks.FARMLAND.defaultBlockState());
                                if(!player.isCreative()){
                                    if(!unbreakingEnchantment.isEmpty()){
                                        if(unbreakingEnchantment.contains("Unbreaking}=>1}")){
                                            if(random.nextInt(100) + 1 <= 50){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>2}")){
                                            if(random.nextInt(100) + 1 <= 33){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>3}")){
                                            if(random.nextInt(100) + 1 <= 25){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }
                                    }else{
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }

                            }else if(world.getBlockState(newBlockPos).getBlock() == Blocks.COARSE_DIRT){
                                world.setBlockAndUpdate(newBlockPos, Blocks.DIRT.defaultBlockState());
                                if(!player.isCreative()){
                                    if(!unbreakingEnchantment.isEmpty()){
                                        if(unbreakingEnchantment.contains("Unbreaking}=>1}")){
                                            if(random.nextInt(100) + 1 <= 50){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>2}")){
                                            if(random.nextInt(100) + 1 <= 33){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }else if(unbreakingEnchantment.contains("Unbreaking}=>3}")){
                                            if(random.nextInt(100) + 1 <= 25){
                                                stack.setDamageValue(stack.getDamageValue() + 1);
                                            }
                                        }
                                    }else{
                                        stack.setDamageValue(stack.getDamageValue() + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void SuperSwordHitEntity(LivingDamageEvent.Pre event){
        if(event.getSource().getEntity() instanceof ServerPlayer player){
            LivingEntity target = event.getEntity();
            ItemStack stack = player.getMainHandItem();

            if(RegisterItems.superSword.get().asItem().equals(stack.getItem())){
                target.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, 60, 1)); //Weakness
                target.addEffect(new MobEffectInstance(MobEffects.POISON, 60, 1)); //Posion
                target.addEffect(new MobEffectInstance(MobEffects.GLOWING, 60, 1)); //Glowing
            }
        }
    }

    @SubscribeEvent
    public static void ItemCupHitEntity(LivingDamageEvent.Pre event){
        if(event.getSource().getEntity() instanceof ServerPlayer player){
            LivingEntity target = event.getEntity();
            ItemStack stack = player.getMainHandItem();

            if(RegisterItems.cup.get().asItem().equals(stack.getItem())){
                if(target instanceof ServerPlayer){
                    ServerPlayer playerTarget = (ServerPlayer)event.getEntity();
                    if(!playerTarget.getActiveEffects().toString().contains("effect.minecraft.fire_resistance")){
                        if(!playerTarget.isCreative()){
                            playerTarget.igniteForSeconds(3);
                        }
                    }
                }else{
                    target.igniteForSeconds(3);
                }
            }
        }
    }

    @SubscribeEvent
    public static void SpecialCupHitEntity(LivingDamageEvent.Pre event){
        if(event.getSource().getEntity() instanceof ServerPlayer player){
            LivingEntity target = event.getEntity();
            ItemStack stack = player.getMainHandItem();

            if(RegisterItems.specialCup.get().asItem().equals(stack.getItem())){
                target.addEffect(new MobEffectInstance(MobEffects.WITHER, 100, 5)); //Wither
                target.addEffect(new MobEffectInstance(MobEffects.GLOWING, 100, 1)); //Glowing
            }
        }
    }

    private static ArrayList<BlockPos> getWoodNeighbours(Level world, BlockPos blockPos, Block block, ItemStack stack) {
        ArrayList<BlockPos> list = new ArrayList<>();
        for(int x=-1; x<=1; x++){
            if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                stack.shrink(1);
                break;
            }
            for(int y=-1; y<=1; y++){
                if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                    stack.shrink(1);
                    break;
                }
                for(int z=-1; z<=1; z++) {
                    if(stack.getDamageValue() >= stack.getMaxDamage() - 1){
                        stack.shrink(1);
                        break;
                    }
                    BlockPos newBlockPos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                    if(world.getBlockState(newBlockPos).getBlock().equals(block)) {
                        list.add(newBlockPos);
                    }
                }
            }
        }

        return list;
    }

    /**
     * Helper Method for the Super Pickaxe. It Destroys the Blocks on the newBlockPos and drops them. If you have Fortune it drops more of the blocks, if they are ores.
     * If you have Silktouch it drops the Blocks like you would destroy them with silktouch.
     * @param world The World in which the Blocks should be destoryed
     * @param player The Player which is used to check if he is in creative mode
     * @param stack The Itemstack so it can be damaged
     * @param blocksCleared amount of Blocks cleared
     * @param unbreakingEnchantment unbreaking Enchantment String which is used for Damage
     * @param fortuneEnchantment Fortune Enchantment String which is used to determine the amount of Drops
     * @param silktouchEnchantment Silktouch Enchantment which is used to check is the Blocks should drop as their normal Blocks
     * @param newBlockPos Position of the new Block which should be destroyed
     * @param randomNumber random Number which is used to determine the amount of Drops
     * @param random Instance of the Class Random which is used to determine the amount of Damage
     */
    private static void destroyPickaxeBlocks(Level world, Player player, ItemStack stack, int blocksCleared, String unbreakingEnchantment, String fortuneEnchantment, String silktouchEnchantment, BlockPos newBlockPos, int randomNumber, Random random){
        if(world.getBlockState(newBlockPos).is(BlockTags.MINEABLE_WITH_PICKAXE)){
            if (world.getBlockState(newBlockPos).getBlock() != Blocks.BEDROCK && world.getBlockState(newBlockPos).getBlock() != Blocks.END_PORTAL_FRAME) {
                Block blockToDestroy = world.getBlockState(newBlockPos).getBlock();
                if (!silktouchEnchantment.isEmpty()) {
                    if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                        Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                        world.destroyBlock(newBlockPos, false);
                        Block.popResource(world, newBlockPos, new ItemStack(destroyedBlock.asItem()));
                    }else{
                        world.destroyBlock(newBlockPos, false);
                    }
                }else if (!fortuneEnchantment.isEmpty()) {
                    if (Arrays.toString(blockToDestroy.defaultBlockState().getTags().toArray()).contains("c:ores/") && blockToDestroy != Blocks.ANCIENT_DEBRIS) {
                        if (fortuneEnchantment.contains("Fortune}=>1}")) {
                            if (randomNumber <= 33) {
                                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(world.getBlockState(newBlockPos), world, newBlockPos);
                                    Block.dropResources(world.getBlockState(newBlockPos), world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);

                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else {
                                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    world.destroyBlock(newBlockPos, true);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            }
                        } else if (fortuneEnchantment.contains("Fortune}=>2}")) {
                            if (randomNumber <= 25) {
                                if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else if (randomNumber <= 50) {
                                if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else {
                                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    world.destroyBlock(newBlockPos, true);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            }
                        } else if (fortuneEnchantment.contains("Fortune}=>3}")) {
                            if (randomNumber <= 20) {
                                if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else if (randomNumber <= 40) {
                                if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else if (randomNumber <= 60) {
                                if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    Block.dropResources(blockState, world, newBlockPos);
                                    world.destroyBlock(newBlockPos, false);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            } else {
                                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                                    BlockState blockState = world.getBlockState(newBlockPos);
                                    world.destroyBlock(newBlockPos, true);
                                    int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                                    if (amount != 0) {
                                        ServerLevel sWorld = (ServerLevel)world;
                                        destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                                    }
                                }else{
                                    world.destroyBlock(newBlockPos, false);
                                }
                            }
                        }
                    } else {
                        if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                            Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                            BlockState blockState = world.getBlockState(newBlockPos);
                            world.destroyBlock(newBlockPos, true);
                            int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                            if (amount != 0) {
                                ServerLevel sWorld = (ServerLevel)world;
                                destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                            }
                        }else{
                            world.destroyBlock(newBlockPos, false);
                        }
                    }
                } else {
                    if(Config.enable_BlockDropsInCreative.get() ||!player.isCreative()){
                        Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                        BlockState blockState = world.getBlockState(newBlockPos);
                        world.destroyBlock(newBlockPos, true);
                        int amount = destroyedBlock.getExpDrop(blockState, world, newBlockPos, null, player, stack);
                        if (amount != 0) {
                            ServerLevel sWorld = (ServerLevel)world;
                            destroyedBlock.popExperience(sWorld, newBlockPos, amount);
                        }
                    }else{
                        world.destroyBlock(newBlockPos, false);
                    }
                }
            }
            if (!player.isCreative()) {
                if (blocksCleared != 0) {
                    if (!unbreakingEnchantment.isEmpty()) {
                        if (unbreakingEnchantment.contains("Unbreaking}=>1}")) {
                            if (random.nextInt(100) + 1 <= 50) {
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        } else if (unbreakingEnchantment.contains("Unbreaking}=>2}")) {
                            if (random.nextInt(100) + 1 <= 33) {
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        } else if (unbreakingEnchantment.contains("Unbreaking}=>3}")) {
                            if (random.nextInt(100) + 1 <= 25) {
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        }
                    } else {
                        stack.setDamageValue(stack.getDamageValue() + 1);
                    }
                }
            }
        }
    }

    /**
     * Helper Method for the Super Shovel. It Destroys the Blocks on the newBlockPos and drops them.
     * If you have Silktouch it drops the Blocks like you would destroy them with silktouch.
     * @param world The World in which the Blocks should be destoryed
     * @param player The Player which is used to check if he is in creative mode
     * @param stack The Itemstack so it can be damaged
     * @param blocksCleared amount of Blocks cleared
     * @param unbreakingEnchantment unbreaking Enchantment String which is used for Damage
     * @param silktouchEnchantment Silktouch Enchantment which is used to check is the Blocks should drop as their normal Blocks
     * @param newBlockPos Position of the new Block which should be destroyed
     * @param random Instance of the Class Random which is used to determine the amount of Damage
     */
    private static void destroyShovelBlocks(Level world, Player player, ItemStack stack, int blocksCleared, String unbreakingEnchantment, String silktouchEnchantment, BlockPos newBlockPos, Random random){
        if (world.getBlockState(newBlockPos).is(BlockTags.MINEABLE_WITH_SHOVEL)) {
            if (silktouchEnchantment.length() != 0) {
                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                    Block destroyedBlock = world.getBlockState(newBlockPos).getBlock();
                    if(destroyedBlock == Blocks.DIRT_PATH){
                        world.destroyBlock(newBlockPos, false);
                        Block.popResource(world, newBlockPos, new ItemStack(Blocks.DIRT));
                    }else{
                        world.destroyBlock(newBlockPos, false);
                        Block.popResource(world, newBlockPos, new ItemStack(destroyedBlock.asItem()));
                    }
                }else{
                    world.destroyBlock(newBlockPos, false);
                }
            }else{
                if(Config.enable_BlockDropsInCreative.get() || !player.isCreative()){
                    if(world.getBlockState(newBlockPos).getBlock() == Blocks.SNOW){
                        Block.popResource(world, newBlockPos, new ItemStack(Items.SNOWBALL));
                    }
                    world.destroyBlock(newBlockPos, true);
                }else{
                    world.destroyBlock(newBlockPos, false);
                }

            }
            if(!player.isCreative()){
                if(blocksCleared != 0){
                    if(!unbreakingEnchantment.isEmpty()){
                        if(unbreakingEnchantment.contains("Unbreaking}=>1}")){
                            if(random.nextInt(100) + 1 <= 50){
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        }else if(unbreakingEnchantment.contains("Unbreaking}=>2}")){
                            if(random.nextInt(100) + 1 <= 33){
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        }else if(unbreakingEnchantment.contains("Unbreaking}=>3}")){
                            if(random.nextInt(100) + 1 <= 25){
                                stack.setDamageValue(stack.getDamageValue() + 1);
                            }
                        }
                    }else{
                        stack.setDamageValue(stack.getDamageValue() + 1);
                    }
                }
            }
        }
    }

}
