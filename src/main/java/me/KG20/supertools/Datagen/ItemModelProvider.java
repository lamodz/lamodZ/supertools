package me.KG20.supertools.Datagen;

import me.KG20.supertools.Armor.ItemArmor;
import me.KG20.supertools.Init.RegisterItems;
import me.KG20.supertools.Main.Constants;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.PackType;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.item.armortrim.TrimMaterials;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.registries.DeferredItem;

import java.util.LinkedHashMap;

public class ItemModelProvider extends net.neoforged.neoforge.client.model.generators.ItemModelProvider {

    private static LinkedHashMap<ResourceKey<TrimMaterial>, Float> trimMaterials = new LinkedHashMap<>();
    static {
        trimMaterials.put(TrimMaterials.QUARTZ, 0.1F);
        trimMaterials.put(TrimMaterials.IRON, 0.2F);
        trimMaterials.put(TrimMaterials.NETHERITE, 0.3F);
        trimMaterials.put(TrimMaterials.REDSTONE, 0.4F);
        trimMaterials.put(TrimMaterials.COPPER, 0.5F);
        trimMaterials.put(TrimMaterials.GOLD, 0.6F);
        trimMaterials.put(TrimMaterials.EMERALD, 0.7F);
        trimMaterials.put(TrimMaterials.DIAMOND, 0.8F);
        trimMaterials.put(TrimMaterials.LAPIS, 0.9F);
        trimMaterials.put(TrimMaterials.AMETHYST, 1.0F);
    }
    public ItemModelProvider(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, Constants.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {

        basicItem(RegisterItems.adarum.get());

        handheldItem(RegisterItems.woodenSickle);
        handheldItem(RegisterItems.stoneSickle);
        handheldItem(RegisterItems.ironSickle);
        handheldItem(RegisterItems.goldenSickle);
        handheldItem(RegisterItems.diamondSickle);
        handheldItem(RegisterItems.netheriteSickle);
        handheldItem(RegisterItems.quartzSickle);
        handheldItem(RegisterItems.copperSickle);
        handheldItem(RegisterItems.emeraldSickle);
        handheldItem(RegisterItems.obsidianSickle);
        handheldItem(RegisterItems.lapisSickle);
        handheldItem(RegisterItems.redstoneSickle);


        handheldItem(RegisterItems.quartzAxe);
        handheldItem(RegisterItems.quartzPickaxe);
        handheldItem(RegisterItems.quartzShovel);
        handheldItem(RegisterItems.quartzSword);
        handheldItem(RegisterItems.quartzHoe);

        handheldItem(RegisterItems.copperAxe);
        handheldItem(RegisterItems.copperPickaxe);
        handheldItem(RegisterItems.copperShovel);
        handheldItem(RegisterItems.copperSword);
        handheldItem(RegisterItems.copperHoe);

        handheldItem(RegisterItems.emeraldAxe);
        handheldItem(RegisterItems.emeraldPickaxe);
        handheldItem(RegisterItems.emeraldShovel);
        handheldItem(RegisterItems.emeraldSword);
        handheldItem(RegisterItems.emeraldHoe);

        handheldItem(RegisterItems.obsidianAxe);
        handheldItem(RegisterItems.obsidianPickaxe);
        handheldItem(RegisterItems.obsidianShovel);
        handheldItem(RegisterItems.obsidianSword);
        handheldItem(RegisterItems.obsidianHoe);

        handheldItem(RegisterItems.lapisAxe);
        handheldItem(RegisterItems.lapisPickaxe);
        handheldItem(RegisterItems.lapisShovel);
        handheldItem(RegisterItems.lapisSword);
        handheldItem(RegisterItems.lapisHoe);

        handheldItem(RegisterItems.redstoneAxe);
        handheldItem(RegisterItems.redstonePickaxe);
        handheldItem(RegisterItems.redstoneShovel);
        handheldItem(RegisterItems.redstoneSword);
        handheldItem(RegisterItems.redstoneHoe);

        handheldItem(RegisterItems.boneMealTool);
        handheldItem(RegisterItems.superHoe);
        handheldItem(RegisterItems.superAxe);
        handheldItem(RegisterItems.superPickaxe);
        handheldItem(RegisterItems.superShovel);
        handheldItem(RegisterItems.superSword);
        handheldItem(RegisterItems.cup);
        handheldItem(RegisterItems.specialCup);
        handheldItem(RegisterItems.superSickle);

        trimmedArmorItem(RegisterItems.copperBoots);
        trimmedArmorItem(RegisterItems.copperLeggings);
        trimmedArmorItem(RegisterItems.copperChestplate);
        trimmedArmorItem(RegisterItems.copperHelmet);

        trimmedArmorItem(RegisterItems.obsidianBoots);
        trimmedArmorItem(RegisterItems.obsidianLeggings);
        trimmedArmorItem(RegisterItems.obsidianChestplate);
        trimmedArmorItem(RegisterItems.obsidianHelmet);

        trimmedArmorItem(RegisterItems.lapisBoots);
        trimmedArmorItem(RegisterItems.lapisLeggings);
        trimmedArmorItem(RegisterItems.lapisChestplate);
        trimmedArmorItem(RegisterItems.lapisHelmet);

        trimmedArmorItem(RegisterItems.quartzBoots);
        trimmedArmorItem(RegisterItems.quartzLeggings);
        trimmedArmorItem(RegisterItems.quartzChestplate);
        trimmedArmorItem(RegisterItems.quartzHelmet);

        trimmedArmorItem(RegisterItems.emeraldBoots);
        trimmedArmorItem(RegisterItems.emeraldLeggings);
        trimmedArmorItem(RegisterItems.emeraldChestplate);
        trimmedArmorItem(RegisterItems.emeraldHelmet);

    }

    private void trimmedArmorItem(DeferredItem<ArmorItem> itemDeferredItem) {
        final String MOD_ID = Constants.MODID;

        if(itemDeferredItem.get() instanceof ItemArmor armorItem) {
            trimMaterials.forEach((trimMaterial, value) -> {
                float trimValue = value;
                String armorMaterial = (armorItem.getMaterial().getDelegate().getRegisteredName().replaceAll("[a-z]*:", ""));
                String armorType = switch (armorItem.getEquipmentSlot()) {
                    case HEAD -> "helmet";
                    case CHEST -> "chestplate";
                    case LEGS -> "leggings";
                    case FEET -> "boots";
                    default -> "";
                };

                String armorItemPath = armorItem.toString();
                String trimPath = "trims/items/" + armorType + "_trim_" + trimMaterial.location().getPath();
                String currentTrimName = armorItemPath + "_" + trimMaterial.location().getPath() + "_trim";
                ResourceLocation armorItemResLoc = ResourceLocation.parse(armorItemPath);
                ResourceLocation trimResLoc = ResourceLocation.parse(trimPath); // minecraft namespace
                ResourceLocation trimNameResLoc = ResourceLocation.parse(currentTrimName);

                // This is used for making the ExistingFileHelper acknowledge that this texture exist, so this will
                // avoid an IllegalArgumentException
                existingFileHelper.trackGenerated(trimResLoc, PackType.CLIENT_RESOURCES, ".png", "textures");

                // Trimmed armorItem files
                getBuilder(currentTrimName)
                        .parent(new ModelFile.UncheckedModelFile("item/generated"))
                        .texture("layer0", armorItemResLoc.getNamespace() + ":item/" + armorItemResLoc.getPath())
                        .texture("layer1", trimResLoc);

                // Non-trimmed armorItem file (normal variant)
                this.withExistingParent(itemDeferredItem.getId().getPath(),
                                mcLoc("item/generated"))
                        .override()
                        .model(new ModelFile.UncheckedModelFile(trimNameResLoc.getNamespace()  + ":item/" + trimNameResLoc.getPath()))
                        .predicate(mcLoc("trim_type"), trimValue).end()
                        .texture("layer0",
                                ResourceLocation.fromNamespaceAndPath(MOD_ID,
                                        "item/" + itemDeferredItem.getId().getPath()));
            });
        }
    }

    private ItemModelBuilder handheldItem(DeferredItem<?> item) {
        return withExistingParent(item.getId().getPath(),
                ResourceLocation.parse("item/handheld")).texture("layer0",
                ResourceLocation.fromNamespaceAndPath(Constants.MODID,"item/" + item.getId().getPath()));
    }
}
