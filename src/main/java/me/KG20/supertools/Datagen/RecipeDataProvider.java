package me.KG20.supertools.Datagen;

import me.KG20.supertools.Init.RegisterItems;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Items;
import net.neoforged.neoforge.common.conditions.IConditionBuilder;

import java.util.concurrent.CompletableFuture;

public class RecipeDataProvider extends RecipeProvider implements IConditionBuilder {
    public RecipeDataProvider(PackOutput pOutput, CompletableFuture<HolderLookup.Provider> pRegistries) {
        super(pOutput, pRegistries);
    }


    @Override
    protected void buildRecipes(RecipeOutput RecipeOutput) {
        /*************************************************************************/
        /**************************SUPER TOOLS************************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superPickaxe.get())
                .pattern("GAG")
                .pattern(" B ")
                .pattern(" B ")
                .define('A', RegisterItems.adarum.get())
                .define('G', Items.GOLD_INGOT)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superShovel.get())
                .pattern("A")
                .pattern("B")
                .pattern("B")
                .define('A', RegisterItems.adarum.get())
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superAxe.get())
                .pattern(" GA")
                .pattern(" BG")
                .pattern(" B ")
                .define('A', RegisterItems.adarum.get())
                .define('G', Items.GOLD_INGOT)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superHoe.get())
                .pattern(" AG")
                .pattern(" B ")
                .pattern(" B ")
                .define('A', RegisterItems.adarum.get())
                .define('G', Items.GOLD_INGOT)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superSword.get())
                .pattern("G")
                .pattern("A")
                .pattern("B")
                .define('A', RegisterItems.adarum.get())
                .define('G', Items.GOLD_INGOT)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.superSickle.get())
                .pattern(" G ")
                .pattern("  G")
                .pattern("BA ")
                .define('A', RegisterItems.adarum.get())
                .define('G', Items.GOLD_INGOT)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_adarum", has(RegisterItems.adarum.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.cup.get())
                .pattern("G G")
                .pattern("GDG")
                .pattern("DDD")
                .define('D', Items.DIAMOND)
                .define('G', Items.GOLD_INGOT)
                .unlockedBy("has_diamond", has(Items.DIAMOND))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.specialCup.get())
                .pattern("GNG")
                .pattern("BCB")
                .pattern("DDD")
                .define('D', Items.DIAMOND)
                .define('G', Items.GOLD_INGOT)
                .define('N', Items.NETHER_STAR)
                .define('C', RegisterItems.cup)
                .define('B', Items.BLAZE_ROD)
                .unlockedBy("has_cup", has(RegisterItems.cup.get()))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.adarum.get())
                .pattern("GDG")
                .pattern("DDD")
                .pattern("GDG")
                .define('D', Items.DIAMOND)
                .define('G', Items.GOLD_INGOT)
                .unlockedBy("has_diamond", has(Items.DIAMOND))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.boneMealTool.get())
                .pattern("LLL")
                .pattern("LBL")
                .pattern("LLL")
                .define('L', Items.LEATHER)
                .define('B', Items.BONE_BLOCK)
                .unlockedBy("has_bone_block", has(Items.BONE_BLOCK))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************QUARTZ TOOLS***********************************/
        /*************************************************************************/


        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzPickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************LAPIS TOOLS************************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisPickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************OBSIDIAN TOOLS*********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianPickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************REDSTONE TOOLS*********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstonePickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstoneAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstoneHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstoneShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstoneSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************EMERALD TOOLS**********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldPickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************COPPER TOOLS***********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperPickaxe.get())
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperAxe.get())
                .pattern(" ##")
                .pattern(" S#")
                .pattern(" S ")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperHoe.get())
                .pattern(" ##")
                .pattern(" S ")
                .pattern(" S ")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperShovel.get())
                .pattern("#")
                .pattern("S")
                .pattern("S")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperSword.get())
                .pattern("#")
                .pattern("#")
                .pattern("S")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************SICKELS****************************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.woodenSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', ItemTags.PLANKS)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(ItemTags.PLANKS))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.stoneSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', ItemTags.STONE_TOOL_MATERIALS)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(ItemTags.STONE_TOOL_MATERIALS))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.ironSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.IRON_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.IRON_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.goldenSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.GOLD_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.GOLD_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.diamondSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.DIAMOND)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.DIAMOND))
                .save(RecipeOutput);

        netheriteSmithing(RecipeOutput, RegisterItems.diamondSickle.get(), RecipeCategory.MISC, RegisterItems.netheriteSickle.get());

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.COPPER_INGOT)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.EMERALD)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.OBSIDIAN)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.LAPIS_LAZULI)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.QUARTZ)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.redstoneSickle.get())
                .pattern(" # ")
                .pattern("  #")
                .pattern("S# ")
                .define('#', Items.REDSTONE)
                .define('S', Items.STICK)
                .unlockedBy("has_main_item", has(Items.REDSTONE))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************QUARTZ ARMOR***********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzHelmet.get())
                .pattern("###")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.QUARTZ)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzChestplate.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .define('#', Items.QUARTZ)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzLeggings.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .define('#', Items.QUARTZ)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.quartzBoots.get())
                .pattern("# #")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.QUARTZ)
                .unlockedBy("has_main_item", has(Items.QUARTZ))
                .save(RecipeOutput);


        /*************************************************************************/
        /**************************LAPIS ARMOR************************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisHelmet.get())
                .pattern("###")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.LAPIS_LAZULI)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisChestplate.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .define('#', Items.LAPIS_LAZULI)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisLeggings.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .define('#', Items.LAPIS_LAZULI)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.lapisBoots.get())
                .pattern("# #")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.LAPIS_LAZULI)
                .unlockedBy("has_main_item", has(Items.LAPIS_LAZULI))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************OBSIDIAN ARMOR*********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianHelmet.get())
                .pattern("###")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.OBSIDIAN)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianChestplate.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .define('#', Items.OBSIDIAN)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianLeggings.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .define('#', Items.OBSIDIAN)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.obsidianBoots.get())
                .pattern("# #")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.OBSIDIAN)
                .unlockedBy("has_main_item", has(Items.OBSIDIAN))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************EMERALD ARMOR**********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldHelmet.get())
                .pattern("###")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.EMERALD)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldChestplate.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .define('#', Items.EMERALD)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldLeggings.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .define('#', Items.EMERALD)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.emeraldBoots.get())
                .pattern("# #")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.EMERALD)
                .unlockedBy("has_main_item", has(Items.EMERALD))
                .save(RecipeOutput);

        /*************************************************************************/
        /**************************COPPER ARMOR***********************************/
        /*************************************************************************/

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperHelmet.get())
                .pattern("###")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.COPPER_INGOT)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperChestplate.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .define('#', Items.COPPER_INGOT)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperLeggings.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .define('#', Items.COPPER_INGOT)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RegisterItems.copperBoots.get())
                .pattern("# #")
                .pattern("# #")
                .pattern("   ")
                .define('#', Items.COPPER_INGOT)
                .unlockedBy("has_main_item", has(Items.COPPER_INGOT))
                .save(RecipeOutput);

    }
}
