package me.KG20.supertools.Datagen;

import me.KG20.supertools.Init.RegisterItems;
import me.KG20.supertools.Main.Constants;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

import javax.annotation.Nullable;
import java.util.concurrent.CompletableFuture;

public class ItemTagProvider extends ItemTagsProvider {
    public ItemTagProvider(PackOutput pOutput, CompletableFuture<HolderLookup.Provider> pLookupProvider,
                           CompletableFuture<TagLookup<Block>> pBlockTags, @Nullable ExistingFileHelper existingFileHelper) {
        super(pOutput, pLookupProvider, pBlockTags, Constants.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider pProvider) {
        this.tag(ItemTags.TRIMMABLE_ARMOR)
                .add(RegisterItems.copperBoots.get())
                .add(RegisterItems.copperLeggings.get())
                .add(RegisterItems.copperChestplate.get())
                .add(RegisterItems.copperHelmet.get())
                .add(RegisterItems.obsidianBoots.get())
                .add(RegisterItems.obsidianLeggings.get())
                .add(RegisterItems.obsidianChestplate.get())
                .add(RegisterItems.obsidianHelmet.get())
                .add(RegisterItems.quartzBoots.get())
                .add(RegisterItems.quartzLeggings.get())
                .add(RegisterItems.quartzChestplate.get())
                .add(RegisterItems.quartzHelmet.get())
                .add(RegisterItems.emeraldBoots.get())
                .add(RegisterItems.emeraldLeggings.get())
                .add(RegisterItems.emeraldChestplate.get())
                .add(RegisterItems.emeraldHelmet.get())
                .add(RegisterItems.lapisBoots.get())
                .add(RegisterItems.lapisLeggings.get())
                .add(RegisterItems.lapisChestplate.get())
                .add(RegisterItems.lapisHelmet.get());

        tag(ItemTags.SHOVELS)
                .add(RegisterItems.copperShovel.get())
                .add(RegisterItems.lapisShovel.get())
                .add(RegisterItems.quartzShovel.get())
                .add(RegisterItems.redstoneShovel.get())
                .add(RegisterItems.obsidianShovel.get())
                .add(RegisterItems.emeraldShovel.get())
                .add(RegisterItems.superShovel.get())
                .add(RegisterItems.cup.get())
                .add(RegisterItems.specialCup.get());

        tag(ItemTags.SWORDS)
                .add(RegisterItems.copperSword.get())
                .add(RegisterItems.lapisSword.get())
                .add(RegisterItems.quartzSword.get())
                .add(RegisterItems.redstoneSword.get())
                .add(RegisterItems.obsidianSword.get())
                .add(RegisterItems.emeraldSword.get())
                .add(RegisterItems.superSword.get())
                .add(RegisterItems.cup.get())
                .add(RegisterItems.specialCup.get());

        tag(ItemTags.PICKAXES)
                .add(RegisterItems.copperPickaxe.get())
                .add(RegisterItems.lapisPickaxe.get())
                .add(RegisterItems.quartzPickaxe.get())
                .add(RegisterItems.redstonePickaxe.get())
                .add(RegisterItems.obsidianPickaxe.get())
                .add(RegisterItems.emeraldPickaxe.get())
                .add(RegisterItems.superPickaxe.get())
                .add(RegisterItems.cup.get())
                .add(RegisterItems.specialCup.get());

        tag(ItemTags.AXES)
                .add(RegisterItems.copperAxe.get())
                .add(RegisterItems.lapisAxe.get())
                .add(RegisterItems.quartzAxe.get())
                .add(RegisterItems.redstoneAxe.get())
                .add(RegisterItems.obsidianAxe.get())
                .add(RegisterItems.emeraldAxe.get())
                .add(RegisterItems.superAxe.get())
                .add(RegisterItems.cup.get())
                .add(RegisterItems.specialCup.get());

        tag(ItemTags.HOES)
                .add(RegisterItems.copperHoe.get())
                .add(RegisterItems.lapisHoe.get())
                .add(RegisterItems.quartzHoe.get())
                .add(RegisterItems.redstoneHoe.get())
                .add(RegisterItems.obsidianHoe.get())
                .add(RegisterItems.emeraldHoe.get())
                .add(RegisterItems.superHoe.get());

        tag(ItemTags.HEAD_ARMOR)
                .add(RegisterItems.copperHelmet.get())
                .add(RegisterItems.lapisHelmet.get())
                .add(RegisterItems.quartzHelmet.get())
                .add(RegisterItems.obsidianHelmet.get())
                .add(RegisterItems.emeraldHelmet.get());

        tag(ItemTags.CHEST_ARMOR)
                .add(RegisterItems.copperChestplate.get())
                .add(RegisterItems.lapisChestplate.get())
                .add(RegisterItems.quartzChestplate.get())
                .add(RegisterItems.obsidianChestplate.get())
                .add(RegisterItems.emeraldChestplate.get());

        tag(ItemTags.LEG_ARMOR)
                .add(RegisterItems.copperLeggings.get())
                .add(RegisterItems.lapisLeggings.get())
                .add(RegisterItems.quartzLeggings.get())
                .add(RegisterItems.obsidianLeggings.get())
                .add(RegisterItems.emeraldLeggings.get());

        tag(ItemTags.FOOT_ARMOR)
                .add(RegisterItems.copperBoots.get())
                .add(RegisterItems.lapisBoots.get())
                .add(RegisterItems.quartzBoots.get())
                .add(RegisterItems.obsidianBoots.get())
                .add(RegisterItems.emeraldBoots.get());
    }
}

