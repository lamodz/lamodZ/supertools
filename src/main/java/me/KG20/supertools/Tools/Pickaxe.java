package me.KG20.supertools.Tools;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.common.ItemAbility;

public class Pickaxe extends PickaxeItem {

    private final TagKey<Block> blocks =  BlockTags.MINEABLE_WITH_PICKAXE;


    public Pickaxe(Tier material) {
        super(material, new Properties().attributes(PickaxeItem.createAttributes(material, 1.0F, -2.8F)));
    }

    public Pickaxe(Tier material, Properties properties) {
        super(material,  properties.attributes(PickaxeItem.createAttributes(material, 1.0F, -2.8F)));
    }

    @Override
    public boolean canPerformAction(ItemStack stack, ItemAbility toolAction) {
        return ItemAbilities.DEFAULT_PICKAXE_ACTIONS.contains(toolAction);
    }

    /*
    @Override
    public boolean isCorrectToolForDrops(ItemStack stack, BlockState state) {
        return state.is(blocks) && TierSortingRegistry.isCorrectTierForDrops(getTier(), state);
    }*/

}
