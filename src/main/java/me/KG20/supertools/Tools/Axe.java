package me.KG20.supertools.Tools;

import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.*;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.ItemAbility;

public class Axe extends AxeItem {

    public Axe(Tier tierMaterial) {
        super(tierMaterial, new Item.Properties().attributes(AxeItem.createAttributes(tierMaterial, 6.0F, -3.2F)));
    }


    @Override
    public boolean canPerformAction(ItemStack stack, ItemAbility itemAbility) {
        return super.canPerformAction(stack, itemAbility);
    }

    @Override
    public boolean isCorrectToolForDrops(ItemStack pStack, BlockState pState) {
        return super.isCorrectToolForDrops(pStack, pState);
    }
}
