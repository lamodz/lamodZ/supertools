package me.KG20.supertools.Tools;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.common.ItemAbility;

public class Shovel extends ShovelItem {

    private final TagKey<Block> blocks =  BlockTags.MINEABLE_WITH_SHOVEL;

    public Shovel(Tier material) {
        super(material, new Properties().attributes(ShovelItem.createAttributes(material, 1.5F, -3.0F)));
    }

    public Shovel(Tier material, Properties properties) {
        super(material, properties.attributes(ShovelItem.createAttributes(material, 1.5F, -3.0F)));
    }

    @Override
    public boolean canPerformAction(ItemStack stack, ItemAbility toolAction) {
        return ItemAbilities.DEFAULT_SHOVEL_ACTIONS.contains(toolAction);
    }

    /*
    @Override
    public boolean isCorrectToolForDrops(ItemStack stack, BlockState state) {
        return state.is(blocks) && TierSortingRegistry.isCorrectTierForDrops(getTier(), state);
    }
    */
}
