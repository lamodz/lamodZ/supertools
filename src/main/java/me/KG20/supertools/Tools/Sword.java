package me.KG20.supertools.Tools;

import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tier;

public class Sword extends SwordItem {

    public Sword(Tier material, float speed) {
        super(material, new Properties().attributes(SwordItem.createAttributes(material, 3, speed)));
    }

    public Sword(Tier material) {
        super(material, new Properties().attributes(SwordItem.createAttributes(material, 3, -2.4F)));
    }

    public Sword(Tier material, float speed, Properties properties) {
        super(material, properties.attributes(SwordItem.createAttributes(material, 3, speed)));
    }
}
