package me.KG20.supertools.Tools;

import me.KG20.supertools.Init.RegisterItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.SpecialPlantable;

public class Sickle extends Item {


    public Sickle(Properties properties) {
        super(properties);
    }


    @Override
    public boolean mineBlock(ItemStack stack, Level world, BlockState state, BlockPos pos, LivingEntity entityLiving) {
        if(stack.getItem() instanceof Sickle && !RegisterItems.superSickle.get().asItem().equals(stack.getItem())){
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();
            if (checkForPlant(world, pos)){
                for (int x = -1; x < 2; x++) {
                    for (int z = -1; z < 2; z++) {
                        BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                        if (checkForPlant(world, newBlockPos)) {
                            world.destroyBlock(newBlockPos, true);
                            stack.setDamageValue(stack.getDamageValue() + 1);
                            if(stack.getDamageValue() >= stack.getMaxDamage()){
                                stack.shrink(1);
                            }
                        }
                    }
                }
            }

        }else if(RegisterItems.superSickle.get().asItem().equals(stack.getItem())){
            int bx = pos.getX();
            int by = pos.getY();
            int bz = pos.getZ();

            if (checkForPlant(world, pos)){
                for (int x = -4; x < 5; x++) {
                    for (int z = -4; z < 5; z++) {

                        BlockPos newBlockPos = new BlockPos(bx + x, by, bz + z);
                        if (checkForPlant(world, newBlockPos)) {
                            world.destroyBlock(newBlockPos, true);
                            stack.setDamageValue(stack.getDamageValue() + 1);
                            if(stack.getDamageValue() >= stack.getMaxDamage()){
                                stack.shrink(1);
                            }
                        }
                    }
                }
            }
        }
        return super.mineBlock(stack, world, state, pos, entityLiving);
    }

    private boolean checkForPlant(Level world, BlockPos blockPos){
        return world.getBlockState(blockPos).getBlock() instanceof BushBlock;
    }
}
