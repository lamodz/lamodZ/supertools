package me.KG20.supertools.Tools;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.*;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.common.ItemAbility;

public class Hoe extends HoeItem {

    private final TagKey<Block> blocks =  BlockTags.MINEABLE_WITH_HOE;


    public Hoe(Tier material) {
        super(material, new Item.Properties().attributes(HoeItem.createAttributes(material, 0.0F, -3.0F)));
    }


    @Override
    public boolean canPerformAction(ItemStack stack, ItemAbility toolAction) {
        return ItemAbilities.DEFAULT_HOE_ACTIONS.contains(toolAction);
    }

    /*
    @Override
    public boolean isCorrectToolForDrops(ItemStack stack, BlockState state) {
        return state.is(blocks) && TierSortingRegistry.isCorrectTierForDrops(getTier(), state);
    }*/


}
